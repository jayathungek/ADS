import java.util.*;
import java.io.*;

public class HeapSort {
    
    public static void main(String[] args) throws IOException {
	Scanner sc = new Scanner(new File(args[0]));
	int len = 0;
	while (sc.hasNextLine()){
		len++;
		sc.nextLine();
	}
	Heap<String> data = new Heap<String>(len);
	Scanner sc2 = new Scanner(new File(args[0]));
	while (sc2.hasNext()) data.insert(sc2.nextLine());
	String s;
	while(data.size() != 0){
		s = data.removeMin();
		System.out.println(s);
	}
    }
}
//
// takes a filename from the command line
// and outputs that file sorted, one word per line
//
