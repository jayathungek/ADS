
public class Heap <E extends Comparable<E>> {
 
    private Object H[];   // contains the objects in the heap
    private int last;     // index of last element in heap
    private int capacity; // max number of elements in heap  

    public Heap(int n){
	capacity = n;
	H        = new Object[capacity+1];
	last     = 0;
    }
    //
    // create a heap with capacity n
    // NOTE: H is an array of objects
    //       must use casting when delivering the minimum
    //       See min() below.
    //       This must also be done in removeMin()
    //

    public Heap(){this(7);}
    //
    // by default, create a new heap with capacity 7
    //

    @SuppressWarnings("unchecked")
    private int compare(Object x,Object y){return ((E)x).compareTo((E)y);}
    
    public int size(){return last;}
    //
    // returns the number of elements in the heap
    //
    
    public boolean isEmpty(){return last==0;}
    //
    // is the heap empty?
    //
    
    @SuppressWarnings("unchecked")
    public E min() throws HeapException {
	if (isEmpty()) throw new HeapException("underflow");
	return (E) H[1];
    }
    //
    // returns element with smallest key, without removal
    // NOTE: must use casting to class (E)
    //
	

    public void swap(int node1, int node2){
        Object temp = H[node1];
        H[node1] = H[node2];
        H[node2] = temp;
    }
    
    public void insert(E e) throws HeapException {
        if (last==capacity) throw new HeapException("overflow");
        last++;
        H[last]=e;
        if (last > 1) bubbleUp();
    }
    // inserts e into the heap
    // throws exception if heap overflow
    //

    
    public void bubbleUp(){
        int index = last; 
        while (index > 1){
            int parent = index/2;
            Object parentNode = H[parent];
            Object childNode = H[index];
            if(compare(childNode,parentNode) >= 0) return;            
            swap(index,parent);
            index = parent;            
        }
    }

    public void bubbleDown(){
        int index = 1; 
        while (true){
            if(index*2 > size()) return;
            int left = index*2;
            int right = index*2 + 1;
            Object current = H[index];
            Object leftChild = H[left];
            Object rightChild = H[right];                
            if(rightChild != null){
                if (compare(current,leftChild) < 0 && compare(current,rightChild) < 0) return;
                if(compare(leftChild,rightChild) < 0) {
                    swap(index,left);
                    index = left;                   
                } else  {
                    swap(index,right);
                    index = right;
                }
            }else{
                if(compare(current,leftChild) < 0) return;
                swap(index,left);
                index = left;
            }                            
        }     
    }
    

    
    @SuppressWarnings("unchecked")
    public E removeMin() throws HeapException {
        if (isEmpty()) throw new HeapException("underflow");
        Object smallest = min();
        H[1] = H[last];
        H[last] = null;
        last--;
        bubbleDown();
        return (E)smallest;
    }
    //
    // removes and returns smallest element of the heap
    // throws exception if heap is empty
    // NOTE: must cast result to class (E)
    //       see min() above
    //

    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("[");
        for (int i=1; i<=last; i++){         
            if (i >1) s.append(",");
            s.append(H[i]);
        }
        s.append("]");
    	return s.toString();
    }
    //
    // outputs the entries in H in the order H[1] to H[last]
    // in same style as used in ArrayQueue
    // 
    // 
    
}

