import java.util.*;
import java.io.*;

public class ArrayQueue<E> {

    public static final int CAPACITY = 10;  // default queue capacity
    private E[] Q;                          // E array used to implement the queue  
    private int n;                          // actual capacity of queue
    private int front;                      // index for the top of queue
    private int rear;                       // rear of the queue
    private int size;                       // size of the queue
    
	
    public ArrayQueue(){this(CAPACITY);}

    @SuppressWarnings("unchecked")
    public ArrayQueue(int capacity){
	n = capacity;
	Q = (E[]) new Object [n]; 
	front = rear = size = 0;
    }
    //
    // NOTE: java does not allow creation of array with parametrised type!
    //
	
    public int size() {return size;}

	 
    public boolean isEmpty(){return size == 0;}


    public E front() throws ArrayQueueException {
        if (isEmpty())throw new ArrayQueueException("Queue is empty");
        return Q[front];}

	
    public void enqueue(E element) throws ArrayQueueException {
        if(size >= n)throw new ArrayQueueException("Queue Overflow");
        size++;        
        Q[rear] = element;
        rear = (rear+1)%n;
    }


    
    public E dequeue() throws ArrayQueueException {
        if(isEmpty())throw new ArrayQueueException("Queue Underflow");
        E result = Q[front];
        front = (front+1)%n;
        size--;

        return result;}
    //
   
    //
    
    public String toString(){        
		StringBuilder string = new StringBuilder().append("[");
        if(size == 1)string.append(Q[front]);
        else{
            string.append(Q[front]);
            for(int i = front+1; i<(front+size); i++){            
                string.append(",");
				string.append(Q[i%n]);
        }}
        return string.append("]").toString();
        }
    //
    // NOTE: if the queue contains 1,2,3 then return "[1,2,3]"
    //       if the queue contains 1 then return "[1]"
    //       if the queue is empty return "[]"
    //
}
	
