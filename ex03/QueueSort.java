import java.util.*;
import java.io.*;



public class QueueSort<E extends Comparable<E>> {

    private ArrayQueue<ArrayQueue<E>> Q;
    public static final int CAPACITY = 10; 
    private int n;    
    private boolean trace;

	
    public QueueSort(){this(CAPACITY);}
	
    public QueueSort(int capacity){
	n = capacity;
	Q = new ArrayQueue<ArrayQueue<E>>(n);
    }

    private ArrayQueue<E> merge(ArrayQueue<E> q1,ArrayQueue<E> q2) throws ArrayQueueException {
	
    ArrayQueue<E> mergedQueue = new ArrayQueue<E>(q1.size()+q2.size());
    
    while (!q1.isEmpty() && !q2.isEmpty()){        
        if (q1.front().compareTo(q2.front()) < 0) mergedQueue.enqueue(q1.dequeue());
		//If the first element of q1 is smaller than that of q2, it dequeues q1
		//It then enqueues this onto the final list.             
        else mergedQueue.enqueue(q2.dequeue());
		//If the first element of q2 is smaller than that of q1, it dequeues q2
		//It then enqueues this onto the final list. 

    } 

    //If the 2 lists are of different size, there will be some sorted elements left over in
    //one of the lists.	
    if (q1.size() != 0){
        //If this list is q1, the for loop enqueues the remaining elements onto the final sorted list	
        for(int i = 0; i <q1.size(); i++){
            mergedQueue.enqueue(q1.dequeue());            
        }
    }else{
        //If it's q2, it enqueues the rest of q2 onto the final sorted list	
        for(int i = 0; i < q2.size(); i++){
            mergedQueue.enqueue(q2.dequeue());            
        }
    }    
    return mergedQueue;	
    }
    
    public void sort(){        
        while (Q.size() > 1){
            Q.enqueue(merge(Q.dequeue(),Q.dequeue()));
        }    
    }

    public void add(E element){
        ArrayQueue<E> AQ = new ArrayQueue<E>(1);
        AQ.enqueue(element);
        Q.enqueue(AQ);        
    }  
    
    public String toString(){return Q.toString();}
    public void trace(){trace = !trace;}
    public static void main(String[] args) throws IOException {
	Scanner sc = new Scanner(new File(args[0]));
	ArrayList<String> data = new ArrayList<String>();
	while (sc.hasNext()) data.add(sc.next());
	int n = data.size();
    QueueSort<String> QS = new QueueSort<String>(n);
	for (String s : data) QS.add(s);
    if (args.length > 1) QS.trace();
	QS.sort();
	System.out.println(QS);
    }
}
